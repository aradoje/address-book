Address Book application
========================

## Setup

* Clone application:
    * Run `git clone git@gitlab.com:aradoje/address-book.git`

* Install dependencies:
    * Run `composer install`
    
* Set file permissions:
    * https://symfony.com/doc/3.4/setup/file_permissions.html
* Create dev database:
    * Run `bin/console doctrine:database:create`
    * Run `bin/console doctrine:schema:create`
    
* Run webserver
    * Run `bin/console server:run`

* Visit http://127.0.0.1:8000/contacts/index

## Tests
* Create test database
    * Run `bin/console doctrine:database:create --env=test`
    * Run `bin/console doctrine:schema:create --env=test`
   
* Run tests
    * Run `vendor/bin/simple-phpunit`
<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Contact;
use AppBundle\Form\CreateContactType;
use AppBundle\Form\UpdateContactType;
use AppBundle\Message\Command\CreateContact;
use AppBundle\Message\Command\CreateContactHandler;
use AppBundle\Message\Command\DeleteContact;
use AppBundle\Message\Command\DeleteContactHandler;
use AppBundle\Message\Command\UpdateContact;
use AppBundle\Message\Command\UpdateContactHandler;
use Doctrine\ORM\EntityManagerInterface;
use Ramsey\Uuid\Uuid;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class ContactController extends Controller
{
    /**
     * @var EntityManagerInterface
     */
    private EntityManagerInterface $em;
    /**
     * @var CreateContactHandler
     */
    private CreateContactHandler $createContactHandler;
    /**
     * @var UpdateContactHandler
     */
    private UpdateContactHandler $updateContactHandler;
    /**
     * @var DeleteContactHandler
     */
    private DeleteContactHandler $deleteContactHandler;

    public function __construct(
        EntityManagerInterface  $em,
        CreateContactHandler $createContactHandler,
        UpdateContactHandler $updateContactHandler,
        DeleteContactHandler $deleteContactHandler
    ) {
        $this->em = $em;
        $this->createContactHandler = $createContactHandler;
        $this->updateContactHandler = $updateContactHandler;
        $this->deleteContactHandler = $deleteContactHandler;
    }

    /**
     * @Route("/contacts/index", name="contacts_index", methods={"GET"})
     */
    public function indexAction(Request $request)
    {
        $contacts = $this->em->getRepository(Contact::class)->findAll();
        return $this->render('contact/index.html.twig', [
            'contacts' => $contacts,
        ]);
    }

    /**
     * @Route("/contacts/new", name="contacts_new", methods={"GET","POST"})
     */
    public function newAction(Request $request)
    {
        $command = CreateContact::create($uuid = Uuid::uuid4()->toString());
        $form = $this->createForm(CreateContactType::class, $command);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->createContactHandler->handle($command);
            //TODO add flash
            return $this->redirectToRoute('contacts_index');
        }
        // replace this example code with whatever you need
        return $this->render('contact/new.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/contacts/{uuid}/edit", name="contacts_edit", methods={"GET","POST"})
     */
    public function editAction(Request $request, Contact $contact)
    {
        $command = UpdateContact::fromContact($contact);
        $form = $this->createForm(UpdateContactType::class, $command);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->updateContactHandler->handle($command);
            //TODO add flash
            return $this->redirectToRoute('contacts_index');
        }
        // replace this example code with whatever you need
        return $this->render('contact/edit.html.twig', [
            'contact' => $contact,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/contacts/{uuid}/delete", name="contacts_delete", methods={"GET"})
     */
    public function deleteAction(Request $request, Contact $contact)
    {
        $command = DeleteContact::fromContact($contact);
        $this->deleteContactHandler->handle($command);
        //TODO add flash
        return $this->redirectToRoute('contacts_index');
    }
}

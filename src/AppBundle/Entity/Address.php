<?php

declare(strict_types=1);

namespace AppBundle\Entity;

use Assert\Assertion;

/**
 * @author Radoje Albijanic <radoje.albijanic@gmail.com>
 */
class Address
{
    private $streetAndNumber;
    private $zip;
    private $city;
    private $country;

    private function __construct(
        string $streetAndNumber,
        string $zip,
        string $city,
        string $country
    ) {
        Assertion::notEmpty($streetAndNumber);
        Assertion::notEmpty($zip);
        Assertion::notEmpty($city);
        Assertion::notEmpty($country);

        $this->streetAndNumber = $streetAndNumber;
        $this->zip = $zip;
        $this->city = $city;
        $this->country = $country;
    }

    public static function create(
        string $streetAndNumber,
        string $zip,
        string $city,
        string $country
    ): self {
        return new self($streetAndNumber, $zip, $city, $country);
    }

    public function getStreetAndNumber(): string
    {
        return $this->streetAndNumber;
    }

    public function getZip(): string
    {
        return $this->zip;
    }

    public function getCity(): string
    {
        return $this->city;
    }

    public function getCountry(): string
    {
        return $this->country;
    }
}

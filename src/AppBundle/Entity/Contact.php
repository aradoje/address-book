<?php

declare(strict_types=1);

namespace AppBundle\Entity;

use Assert\Assertion;
use Doctrine\ORM\Mapping as ORM;
use Ramsey\Uuid\UuidInterface;

/**
 * @ORM\Entity(repositoryClass="AppBundle\Repository\ContactRepository")
 */
class Contact
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @var UuidInterface
     * @ORM\Column(type="uuid")
     */
    private $uuid;

    /**
     * @var string
     * @ORM\Column(type="string", length=255)
     */
    private $firstName;

    /**
     * @var string
     * @ORM\Column(type="string", length=255)
     */
    private $lastName;

    /**
     * @var string
     * @ORM\Column(type="string", length=255)
     */
    private $streetAndNumber;

    /**
     * @var string
     * @ORM\Column(type="string", length=255)
     */
    private $zip;

    /**
     * @var string
     * @ORM\Column(type="string", length=255)
     */
    private $city;

    /**
     * @var string
     * @ORM\Column(type="string", length=255)
     */
    private $country;

    /**
     * @var string
     * @ORM\Column(type="string", length=255)
     */
    private $phoneNumber;

    /**
     * @var \DateTime
     * @ORM\Column(type="date")
     */
    private $birthday;

    /**
     * @var string
     * @ORM\Column(type="string", length=255)
     */
    private $email;

    /**
     * @var ?string
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $pictureUrl;

    private function __construct(
        UuidInterface $uuid,
        string $firstName,
        string $lastName,
        Address $address,
        string $phoneNumber,
        \DateTime $birthday,
        string $email,
        string $pictureUrl = null
    ) {
        Assertion::notEmpty($firstName);
        Assertion::notEmpty($lastName);
        Assertion::notEmpty($phoneNumber);
        Assertion::notEmpty($email);
        Assertion::email($email);

        $this->uuid = $uuid;
        $this->firstName = $firstName;
        $this->lastName = $lastName;
        $this->streetAndNumber = $address->getStreetAndNumber();
        $this->zip = $address->getZip();
        $this->city = $address->getCity();
        $this->country = $address->getCountry();
        $this->phoneNumber = $phoneNumber;
        $this->birthday = $birthday;
        $this->email = $email;
        $this->pictureUrl = $pictureUrl;
    }

    public static function create(
        UuidInterface $uuid,
        string $firstName,
        string $lastName,
        Address $address,
        string $phoneNumber,
        \DateTime $birthday,
        string $email,
        string $pictureUrl = null
    ): self {
        return new self(
            $uuid,
            $firstName,
            $lastName,
            $address,
            $phoneNumber,
            $birthday,
            $email,
            $pictureUrl
        );
    }

    public function getUuid(): UuidInterface
    {
        return $this->uuid;
    }

    public function getFirstName(): string
    {
        return $this->firstName;
    }

    public function changeFirstName(string $firstName)
    {
        Assertion::notEmpty($firstName);
        $this->firstName = $firstName;
    }

    public function getLastName(): string
    {
        return $this->lastName;
    }

    public function changeLastName(string $lastName)
    {
        Assertion::notEmpty($lastName);
        $this->lastName = $lastName;
    }

    public function getAddress(): Address
    {
        return Address::create(
            $this->streetAndNumber,
            $this->zip,
            $this->city,
            $this->country
        );
    }
    public function changeAddress(Address $address)
    {
        $this->streetAndNumber = $address->getStreetAndNumber();
        $this->zip = $address->getZip();
        $this->city = $address->getCity();
        $this->country = $address->getCountry();
    }

    public function getPhoneNumber(): string
    {
        return $this->phoneNumber;
    }

    public function changePhoneNumber(string $phoneNumber)
    {
        Assertion::notEmpty($phoneNumber);
        $this->phoneNumber = $phoneNumber;
    }

    public function getBirthday(): \DateTime
    {
        return $this->birthday;
    }

    public function changeBirthday(\DateTime $birthday)
    {
        Assertion::notEmpty($birthday);
        $this->birthday = $birthday;
    }

    public function getEmail(): string
    {
        return $this->email;
    }

    public function changeEmail(string $email)
    {
        Assertion::notEmpty($email);
        $this->email = $email;
    }

    public function getPictureUrl()
    {
        return $this->pictureUrl;
    }

    public function changePictureUrl(string $pictureUrl = null)
    {
        $this->pictureUrl = $pictureUrl;
    }
}

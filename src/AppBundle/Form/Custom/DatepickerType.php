<?php

declare(strict_types=1);

namespace AppBundle\Form\Custom;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;

/**
 * @author Radoje Albijanic <radoje.albijanic@gmail.com>
 */
class DatepickerType extends AbstractType
{
    public function getParent(): string
    {
        return TextType::class;
    }
}

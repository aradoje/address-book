<?php

declare(strict_types=1);

namespace AppBundle\Form;

use AppBundle\Form\Custom\DatepickerType;
use AppBundle\Message\Command\CreateContact;
use AppBundle\Message\Command\UpdateContact;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * @author Radoje Albijanic <radoje.albijanic@gmail.com>
 */
class UpdateContactType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('firstName');
        $builder->add('lastName');
        $builder->add('streetAndNumber');
        $builder->add('zip');
        $builder->add('city');
        $builder->add('country');
        $builder->add('phoneNumber');
        $builder->add('birthday', DatepickerType::class);
        $builder->add('email');
        $builder->add('pictureUrl');
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => UpdateContact::class,
        ]);
    }
}

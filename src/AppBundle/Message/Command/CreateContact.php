<?php

declare(strict_types=1);

namespace AppBundle\Message\Command;

use Ramsey\Uuid\UuidInterface;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @author Radoje Albijanic <radoje.albijanic@gmail.com>
 */
class CreateContact
{

    /**
     * @var string
     * @Assert\NotBlank()
     * @Assert\Uuid()
     */
    private $uuid;

    /**
     * @var string
     * @Assert\NotBlank()
     */
    private $firstName;

    /**
     * @var string
     * @Assert\NotBlank()
     */
    private $lastName;

    /**
     * @var string
     * @Assert\NotBlank()
     */
    private $streetAndNumber;

    /**
     * @var string
     * @Assert\NotBlank()
     */
    private $zip;

    /**
     * @var string
     * @Assert\NotBlank()
     */
    private $city;

    /**
     * @var string
     * @Assert\NotBlank()
     */
    private $country;

    /**
     * @var string
     * @Assert\NotBlank()
     */
    private $phoneNumber;

    /**
     * @var string
     * @Assert\NotBlank()
     * @Assert\Date()
     */
    private $birthday;

    /**
     * @var string
     * @Assert\NotBlank()
     * @Assert\Email()
     */
    private $email;

    /**
     * @var ?string
     * @Assert\Url()
     */
    private $pictureUrl;

    private function __construct(string $uuid)
    {
        $this->uuid = $uuid;
    }

    public static function create(string $uuid): self
    {
        return new self($uuid);
    }

    public function getUuid(): string
    {
        return $this->uuid;
    }

    public function getFirstName(): string
    {
        return $this->firstName ?? '';
    }

    public function setFirstName($firstName)
    {
        $this->firstName = (string)$firstName;
    }

    public function getLastName(): string
    {
        return $this->lastName ?? '';
    }

    public function setLastName($lastName)
    {
        $this->lastName = (string)$lastName;
    }

    public function getStreetAndNumber(): string
    {
        return $this->streetAndNumber ?? '';
    }

    public function setStreetAndNumber($streetAndNumber)
    {
        $this->streetAndNumber = $streetAndNumber ?? '';
    }

    public function getZip(): string
    {
        return $this->zip ?? '';
    }

    public function setZip($zip)
    {
        $this->zip = (string)$zip;
    }

    public function getCity(): string
    {
        return $this->city ?? '';
    }

    public function setCity($city)
    {
        $this->city = (string)$city;
    }

    public function getCountry(): string
    {
        return $this->country ?? '';
    }

    public function setCountry($country)
    {
        $this->country = $country;
    }

    public function getPhoneNumber(): string
    {
        return $this->phoneNumber ?? '';
    }

    public function setPhoneNumber($phoneNumber)
    {
        $this->phoneNumber = (string)$phoneNumber;
    }

    public function getBirthday()
    {
        return $this->birthday;
    }

    public function setBirthday($birthday)
    {
        $this->birthday = $birthday;
    }

    public function getEmail(): string
    {
        return $this->email ?? '';
    }

    public function setEmail($email)
    {
        $this->email = $email ?? '';
    }

    public function getPictureUrl()
    {
        return $this->pictureUrl;
    }

    public function setPictureUrl($pictureUrl)
    {
        $this->pictureUrl = $pictureUrl;
    }
}

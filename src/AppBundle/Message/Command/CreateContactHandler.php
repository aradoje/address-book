<?php

declare(strict_types=1);

namespace AppBundle\Message\Command;

use AppBundle\Entity\Address;
use AppBundle\Entity\Contact;
use Doctrine\ORM\EntityManagerInterface;
use Ramsey\Uuid\Nonstandard\Uuid;

/**
 * @author Radoje Albijanic <radoje.albijanic@gmail.com>
 */
class CreateContactHandler
{
    /**
     * @var EntityManagerInterface
     */
    private $em;

    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    public function handle(CreateContact $command)
    {
        $contact = Contact::create(
            Uuid::fromString($command->getUuid()),
            $command->getFirstName(),
            $command->getLastName(),
            Address::create(
                $command->getStreetAndNumber(),
                $command->getZip(),
                $command->getCity(),
                $command->getCountry()
            ),
            $command->getPhoneNumber(),
            \DateTime::createFromFormat('Y-m-d', $command->getBirthday()),
            $command->getEmail(),
            $command->getPictureUrl()
        );

        $this->em->persist($contact);
        $this->em->flush();
    }
}

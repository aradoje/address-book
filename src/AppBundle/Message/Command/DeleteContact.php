<?php

declare(strict_types=1);

namespace AppBundle\Message\Command;

use AppBundle\Entity\Contact;
use Ramsey\Uuid\UuidInterface;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @author Radoje Albijanic <radoje.albijanic@gmail.com>
 */
class DeleteContact
{
    /**
     * @var string
     * @Assert\NotBlank()
     * @Assert\Uuid()
     */
    private $uuid;

    private function __construct(string $uuid)
    {
        $this->uuid = $uuid;
    }

    public static function fromContact(Contact $contact): self
    {
        return new self($contact->getUuid()->toString());
    }

    public function getUuid(): string
    {
        return $this->uuid;
    }
}

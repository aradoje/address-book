<?php

declare(strict_types=1);

namespace AppBundle\Message\Command;

use AppBundle\Entity\Address;
use AppBundle\Entity\Contact;
use Doctrine\ORM\EntityManagerInterface;
use Ramsey\Uuid\Nonstandard\Uuid;

/**
 * @author Radoje Albijanic <radoje.albijanic@gmail.com>
 */
class DeleteContactHandler
{
    /**
     * @var EntityManagerInterface
     */
    private $em;

    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    public function handle(DeleteContact $command)
    {
        $contact = $this->em->getRepository(Contact::class)->findOneByUuid(Uuid::fromString($command->getUuid()));

        $this->em->remove($contact);
        $this->em->flush();
    }
}

<?php

declare(strict_types=1);

namespace AppBundle\Message\Command;

use AppBundle\Entity\Address;
use AppBundle\Entity\Contact;
use Doctrine\ORM\EntityManagerInterface;
use Ramsey\Uuid\Nonstandard\Uuid;

/**
 * @author Radoje Albijanic <radoje.albijanic@gmail.com>
 */
class UpdateContactHandler
{
    /**
     * @var EntityManagerInterface
     */
    private $em;

    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    public function handle(UpdateContact $command)
    {
        $contact = $this->em->getRepository(Contact::class)->findOneByUuid(Uuid::fromString($command->getUuid()));

        $newAddress = Address::create(
            $command->getStreetAndNumber(),
            $command->getZip(),
            $command->getCity(),
            $command->getCountry()
        );

        $contact->changeFirstName($command->getFirstName());
        $contact->changeLastName($command->getLastName());
        $contact->changeAddress($newAddress);
        $contact->changePhoneNumber($command->getPhoneNumber());
        $contact->changeBirthday(\DateTime::createFromFormat('Y-m-d', $command->getBirthday()));
        $contact->changeEmail($command->getEmail());
        $contact->changePictureUrl($command->getPictureUrl());

        $this->em->flush();
    }
}

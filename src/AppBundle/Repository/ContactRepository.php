<?php

namespace AppBundle\Repository;

use AppBundle\Entity\Contact;
use AppBundle\Repository\Exception\EntityNotFoundException;
use Doctrine\ORM\EntityRepository;
use Ramsey\Uuid\UuidInterface;

class ContactRepository extends EntityRepository
{
    /**
     * @throws EntityNotFoundException
     */
    public function findOneByUuid(UuidInterface $uuid): Contact
    {
        $entity = $this->findOneBy(['uuid' => $uuid->toString()]);
        if (!$entity instanceof Contact) {
            throw EntityNotFoundException::entityWithId(Contact::class, $uuid->toString());
        }

        return $entity;
    }

    /**
     * @return Contact[]
     */
    public function findAll(): array
    {
        return $this->createQueryBuilder('c')
            ->orderBy('c.firstName')
            ->getQuery()
            ->getResult();
    }
}

<?php

declare(strict_types=1);

namespace AppBundle\Repository\Exception;

use Exception;

final class EntityNotFoundException extends Exception implements EntityNotFoundExceptionException
{
    public static function withMessage(string $message): self
    {
        return new self($message);
    }

    public static function entityWithId(string $class, $id): self
    {
        return new self(sprintf('Could not find entity of type "%s" with %s.', $class, (string) $id));
    }
}

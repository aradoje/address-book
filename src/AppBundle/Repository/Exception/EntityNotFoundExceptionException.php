<?php

declare(strict_types=1);

namespace AppBundle\Repository\Exception;

/**
 * @author Radoje Albijanic <radoje.albijanic@gmail.com>
 */
interface EntityNotFoundExceptionException extends \Throwable
{
}

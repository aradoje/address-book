<?php

namespace Tests\Functional\AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Tests\Functional\BaseFunctionalTestCase;

class ContactControllerTest extends BaseFunctionalTestCase
{
    public function testIndexOk()
    {
        $this->fixturesFactory->contact();
        $this->fixturesFactory->contact();
        $this->fixturesFactory->contact();
        $this->fixturesFactory->contact();
        $this->fixturesFactory->contact();
        $this->fixturesFactory->contact();
        $this->fixturesFactory->contact();

        $crawler = self::$client->request('GET', '/contacts/index');

        $this->assertEquals(200, self::$client->getResponse()->getStatusCode());
        $this->assertNotEmpty($crawler->filter('#default-datatable'));
    }

    public function testNewOk()
    {
        self::$client->request('GET', '/contacts/new');

        self::assertEquals(200, self::$client->getResponse()->getStatusCode());
    }

    public function testEditOk()
    {
        $contact = $this->fixturesFactory->contact();

        self::$client->request('GET', sprintf('/contacts/%s/edit', $contact->getUuid()->toString()));

        self::assertEquals(200, self::$client->getResponse()->getStatusCode());
    }

    public function testDeleteRedirects()
    {
        $contact = $this->fixturesFactory->contact();

        self::$client->request('GET', sprintf('/contacts/%s/delete', $contact->getUuid()->toString()));

        self::assertEquals(302, self::$client->getResponse()->getStatusCode());
    }
}

<?php

declare(strict_types=1);

namespace Tests\Functional\AppBundle\Message\Command;

use AppBundle\Entity\Contact;
use AppBundle\Message\Command\CreateContact;
use AppBundle\Message\Command\CreateContactHandler;
use Ramsey\Uuid\Uuid;
use Tests\Functional\BaseFunctionalTestCase;

/**
 * @author Radoje Albijanic <radoje.albijanic@gmail.com>
 */
class CreateContactHandlerTest extends BaseFunctionalTestCase
{
    public function testItSavesEntity()
    {
        $handler = self::$container->get(CreateContactHandler::class);

        $command = CreateContact::create($uuid = Uuid::uuid4()->toString());
        $command->setFirstName('FirstName');
        $command->setLastName('LastName');
        $command->setStreetAndNumber('Address');
        $command->setZip('Zip');
        $command->setCity('City');
        $command->setCountry('Country');
        $command->setPhoneNumber('PhoneNumber');
        $command->setBirthday($birthday = '1964-06-09');
        $command->setEmail('email@xx.xx');
        $command->setPictureUrl($pictureUrl = '/picture.jpg');

        $handler->handle($command);
        $this->em->clear();

        $saved = $this->em->getRepository(Contact::class)->findOneBy(['uuid' => $uuid]);

        self::assertInstanceOf(Contact::class, $saved);
        self::assertEquals('FirstName', $saved->getFirstName());
        self::assertEquals('LastName', $saved->getLastName());
        self::assertEquals('Address', $saved->getAddress()->getStreetAndNumber());
        self::assertEquals('Zip', $saved->getAddress()->getZip());
        self::assertEquals('City', $saved->getAddress()->getCity());
        self::assertEquals('Country', $saved->getAddress()->getCountry());
        self::assertEquals('PhoneNumber', $saved->getPhoneNumber());
        self::assertEquals($birthday, $saved->getBirthday()->format('Y-m-d'));
        self::assertEquals($pictureUrl, $saved->getPictureUrl());
    }
}

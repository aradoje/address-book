<?php

declare(strict_types=1);

namespace Tests\Functional\AppBundle\Message\Command;

use AppBundle\Entity\Contact;
use AppBundle\Message\Command\CreateContact;
use AppBundle\Message\Command\CreateContactHandler;
use AppBundle\Message\Command\DeleteContact;
use AppBundle\Message\Command\DeleteContactHandler;
use AppBundle\Message\Command\UpdateContact;
use AppBundle\Message\Command\UpdateContactHandler;
use Ramsey\Uuid\Uuid;
use Tests\Functional\BaseFunctionalTestCase;

/**
 * @author Radoje Albijanic <radoje.albijanic@gmail.com>
 */
class DeleteContactHandlerTest extends BaseFunctionalTestCase
{
    public function testItRemovesEntity()
    {
        $contact = $this->fixturesFactory->contact();
        $command = DeleteContact::fromContact($contact);

        self::$container->get(DeleteContactHandler::class)->handle($command);
        $this->em->clear();

        /** @var Contact $saved */
        $saved = $this->em->getRepository(Contact::class)->findOneBy(['uuid' => $contact->getUuid()->toString()]);

        self::assertNull($saved, 'Contact was not removed from database');
    }
}

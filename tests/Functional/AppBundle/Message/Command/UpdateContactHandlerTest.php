<?php

declare(strict_types=1);

namespace Tests\Functional\AppBundle\Message\Command;

use AppBundle\Entity\Contact;
use AppBundle\Message\Command\CreateContact;
use AppBundle\Message\Command\CreateContactHandler;
use AppBundle\Message\Command\UpdateContact;
use AppBundle\Message\Command\UpdateContactHandler;
use Ramsey\Uuid\Uuid;
use Tests\Functional\BaseFunctionalTestCase;

/**
 * @author Radoje Albijanic <radoje.albijanic@gmail.com>
 */
class UpdateContactHandlerTest extends BaseFunctionalTestCase
{
    public function testItSavesEntity()
    {
        $contact = $this->fixturesFactory->contact();
        $command = UpdateContact::fromContact($contact);
        $command->setFirstName('newFirstName');
        $command->setLastName('newLastName');
        $command->setStreetAndNumber('newAddress');
        $command->setZip('newZip');
        $command->setCity('newCity');
        $command->setCountry('newCountry');
        $command->setPhoneNumber('newPhoneNumber');
        $command->setBirthday($newBirthday = '1986-06-09');
        $command->setEmail('new-email@xx.xx');
        $command->setPictureUrl($newPictureUrl = '/new/picture.jpg');

        self::$container->get(UpdateContactHandler::class)->handle($command);
        $this->em->clear();

        /** @var Contact $saved */
        $saved = $this->em->getRepository(Contact::class)->findOneBy(['uuid' => $contact->getUuid()->toString()]);

        self::assertInstanceOf(Contact::class, $saved);
        self::assertEquals('newFirstName', $saved->getFirstName());
        self::assertEquals('newLastName', $saved->getLastName());
        self::assertEquals('newAddress', $saved->getAddress()->getStreetAndNumber());
        self::assertEquals('newZip', $saved->getAddress()->getZip());
        self::assertEquals('newCity', $saved->getAddress()->getCity());
        self::assertEquals('newCountry', $saved->getAddress()->getCountry());
        self::assertEquals('newPhoneNumber', $saved->getPhoneNumber());
        self::assertEquals($newBirthday, $saved->getBirthday()->format('Y-m-d'));
        self::assertEquals($newPictureUrl, $saved->getPictureUrl());
    }
}

<?php

declare(strict_types=1);

namespace Tests\Functional\Repository;

use AppBundle\Entity\Contact;
use AppBundle\Repository\ContactRepository;
use AppBundle\Repository\Exception\EntityNotFoundException;
use Ramsey\Uuid\Uuid;
use Tests\Functional\BaseFunctionalTestCase;

/**
 * @author Radoje Albijanic <radoje.albijanic@gmail.com>
 */
class ContactRepositoryTest extends BaseFunctionalTestCase
{
    /**
     * @var ContactRepository
     */
    private $repository;

    protected function setUp()
    {
        parent::setUp();
        $this->repository = $this->em->getRepository(Contact::class);
    }

    public function testFindOneByUuid()
    {
        $this->fixturesFactory->contact();
        $toFind = $this->fixturesFactory->contact();
        $this->fixturesFactory->contact();
        $this->fixturesFactory->contact();

        $saved = $this->repository->findOneByUuid($toFind->getUuid());
        self::assertEquals($toFind->getUuid()->toString(), $saved->getUuid()->toString());
    }

    public function testFindOneByUuidRisesExceptionIfNotFound()
    {
        $this->fixturesFactory->contact();
        $this->fixturesFactory->contact();
        $this->fixturesFactory->contact();

        $this->expectException(EntityNotFoundException::class);

        $this->repository->findOneByUuid(Uuid::uuid4());
    }
}

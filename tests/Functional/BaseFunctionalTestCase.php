<?php

declare(strict_types=1);

namespace Tests\Functional;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Symfony\Component\BrowserKit\Client;
use Symfony\Component\DependencyInjection\Container;
use Tests\SharedService\FixturesFactory\FixturesFactory;

/**
 * @author Radoje Albijanic <radoje.albijanic@gmail.com>
 */
class BaseFunctionalTestCase extends KernelTestCase
{
    /**
     * @var EntityManagerInterface
     */
    protected $em;

    /**
     * @var Container
     */
    protected static $container;

    /**
     * @var Client
     */
    protected static $client;

    /**
     * @var FixturesFactory
     */
    protected $fixturesFactory;

    protected function setUp()
    {
        $kernel = static::bootKernel();

        //we need to get all from one container in order to have same entity manager
        $this->em = $kernel->getContainer()->get('doctrine.orm.entity_manager');
        $this->em->beginTransaction();
        $this->fixturesFactory = FixturesFactory::create($this->em);
        self::$client = $kernel->getContainer()->get('test.client');
        self::$container = $kernel->getContainer();
    }
}

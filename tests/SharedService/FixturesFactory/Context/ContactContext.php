<?php

declare(strict_types=1);

namespace Tests\SharedService\FixturesFactory\Context;

use AppBundle\Entity\Address;
use AppBundle\Entity\Contact;
use Ramsey\Uuid\Uuid;
use Tests\SharedService\FixturesFactory\FixturesFactory;

/**
 * @author Radoje Albijanic <radoje.albijanic@gmail.com>
 */
final class ContactContext
{
    public $firstName;
    public $lastName;
    public $streetAndNumber;
    public $zip;
    public $city;
    public $country;
    public $phoneNumber;
    public $birthday;
    public $email;
    public $pictureUrl;

    public function create(FixturesFactory $factory): Contact
    {
        $faker = FixturesFactory::faker();
        return Contact::create(
            Uuid::uuid4(),
            $this->firstName ?? $faker->firstName,
            $this->lastName ?? $faker->lastName,
            Address::create(
                $this->streetAndNumber ?? $faker->streetAddress,
                $this->zip ?? $faker->word,
                $this->city ?? $faker->city,
                $this->country ?? $faker->country,
            ),
            $this->phoneNumber ?? $faker->phoneNumber,
            $this->birthday ?? new \DateTime(),
            $this->email ?? $faker->email,
            $this->pictureUrl ?? $faker->url
        );
    }
}

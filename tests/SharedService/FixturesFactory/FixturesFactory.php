<?php

declare(strict_types=1);

/*
 * This file is part of PHP CS Fixer.
 * (c) Fabien Potencier <fabien@symfony.com>
 *     Dariusz Rumiński <dariusz.ruminski@gmail.com>
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

namespace Tests\SharedService\FixturesFactory;

use AppBundle\Entity\Contact;
use Doctrine\ORM\EntityManagerInterface;
use Faker\Factory;
use Faker\Generator;
use Ramsey\Uuid\Uuid;
use Tests\SharedService\FixturesFactory\Context\ContactContext;

/**
 * Fixtures factory explained in https://medium.com/ticketswap/fixture-factory-in-php-7969efd9c5d7.
 *
 * @author Radoje Albijanic <radoje@blackmountainlabs.me>
 */
final class FixturesFactory
{
    private $contexts = [];
    private $manager;

    private function __construct(EntityManagerInterface $manager = null)
    {
        $this->manager = $manager;
    }

    /**
     * @see https://github.com/fzaninotto/Faker
     */
    public static function faker(): Generator
    {
        return Factory::create();
    }

    public static function getUuidString(): string
    {
        return Uuid::uuid4()->toString();
    }

    public static function create(EntityManagerInterface $manager = null): self
    {
        return new self($manager);
    }

    public function contact(callable $customize = null): Contact
    {
        return $this->insertFixture(ContactContext::class, $customize);
    }

    /**
     * Creates and inserts entities into the database.
     */
    private function insertFixture(string $contextClass, $customize = null)
    {
        $fixture = $this->createFixture($contextClass, $customize);
        if (null !== $this->manager) {
            $this->manager->persist($fixture);
            $this->manager->flush();
        }

        return $fixture;
    }

    /**
     * Creates entities.
     */
    private function createFixture(string $contextClass, $customize = null)
    {
        $context = $this->contexts[$contextClass] ?? new $contextClass();
        if (null !== $customize) {
            $customize($context);
        }

        return $context->create($this);
    }
}
